import axios from 'axios'

const BASE_URL = 'https://jsonplaceholder.typicode.com'

export const fetchPosts = (page: number = 1, query: string) =>
  axios.get(
    `${BASE_URL}/posts?_page=${page}&_limit=25${query ? `&q=${query}` : ''}`,
  )

export const updatePost = (id: number, data: object) =>
  axios.put(`${BASE_URL}/posts/${id}`, data)

export const deletePost = (id: number) =>
  axios.delete(`${BASE_URL}/posts/${id}`)

export const fetchUsers = (query: number | null) =>
  axios.get(`${BASE_URL}/users/${query ? `${query}/posts` : ''}`)
