import { Box, Heading, Text, Button, Stack, Flex } from '@chakra-ui/react'
import { PostType } from 'utils/types'

type Props = {
  post: PostType
  onEditClick: () => void
  onDeleteClick: () => void
}

const Post = ({ post, onEditClick, onDeleteClick }: Props) => {
  const handleEditClick = () => {
    onEditClick()
  }

  const handleDeleteClick = () => {
    onDeleteClick()
  }

  return (
    <Box maxW="xl" shadow="sm" my="2" borderRadius="lg" p="4">
      <Heading size="sm" textAlign="left" fontWeight="bold">
        {post.title}
      </Heading>
      <Text mt="2" noOfLines={2} textAlign="left" fontSize="xs">
        {post.body}
      </Text>
      <Flex direction="row" mt="2" gap="2" justifyContent="end">
        <Button
          variant="outline"
          colorScheme="blue"
          size="sm"
          onClick={handleEditClick}
        >
          Edit
        </Button>
        <Button
          variant="outline"
          colorScheme="red"
          size="sm"
          onClick={handleDeleteClick}
        >
          Delete
        </Button>
      </Flex>
    </Box>
  )
}

export default Post
