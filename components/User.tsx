import { Td, Tr } from '@chakra-ui/react'
import { UserType } from 'utils/types'

type Props = {
  user: UserType
  onClick: () => void
}

const User = ({ user, onClick }: Props) => {
  return (
    <Tr>
      <Td>{user.id}</Td>
      <Td onClick={onClick}>{user.username}</Td>
      <Td>{user.name}</Td>
      <Td>{user.email}</Td>
    </Tr>
  )
}

export default User
