import {
  Box,
  Button,
  Flex,
  Input,
  Tab,
  Table,
  TableContainer,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Tbody,
  Th,
  Thead,
  Tr,
} from '@chakra-ui/react'
import { useQuery } from '@tanstack/react-query'
import { useEffect, useState } from 'react'
import { fetchPosts, fetchUsers } from 'utils/api'
import { useDebounce, usePrevious } from 'utils/hooks'
import { PostType, UserType } from 'utils/types'
import Post from './Post'
import User from './User'

const Posts = () => {
  const [searchTerm, setSearchTerm] = useState('')
  const [currentUser, setCurrentUser] = useState<number | null>(null)
  const [posts, setPosts] = useState<PostType[]>([])
  const [users, setUsers] = useState<UserType[]>([])
  const [page, setPage] = useState(1)
  const debouncedSearchTerm = useDebounce(searchTerm, 500)
  const prevSearchTerm = usePrevious(debouncedSearchTerm)

  const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(e.target.value)
    setPage(1)
    setCurrentUser(null)
  }

  const {
    isLoading: isPostsLoading,
    isFetching: isPostsFetching,
    error: postsError,
    refetch: refetchPosts,
  } = useQuery(['posts', page], () => fetchPosts(page, debouncedSearchTerm), {
    onSuccess: res => {
      setPosts(res.data)
    },
  })

  const {
    isLoading: isUsersLoading,
    error: usersError,
    refetch: refetchUsers,
  } = useQuery(
    ['users', currentUser],
    () => fetchUsers(currentUser ? currentUser : null),
    {
      onSuccess: res => {
        setUsers(res.data)
      },
    },
  )

  useEffect(() => {
    if (prevSearchTerm !== debouncedSearchTerm && !isPostsLoading) {
      refetchPosts()
    }
  }, [debouncedSearchTerm])

  const handlePreviousClick = () => {
    setPage(prevPage => prevPage - 1)
  }

  const handleNextClick = () => {
    setPage(prevPage => prevPage + 1)
  }

  const handleUserClick = (id: number) => {
    setCurrentUser(id)
    // TODO: Show user posts with refetchUsers()
  }

  if (isPostsFetching || isUsersLoading) {
    return <div>Loading...</div>
  }

  if (postsError || usersError) {
    return <div>Error</div>
  }

  return (
    <Flex maxW="3xl" direction="column">
      <Input
        placeholder="Search posts"
        value={searchTerm}
        type="search"
        borderRadius="lg"
        disabled={isPostsFetching}
        marginY="6"
        onChange={handleSearchChange}
      />
      <Tabs align="center">
        <TabList>
          <Tab>Articles</Tab>
          <Tab>Users</Tab>
        </TabList>

        <TabPanels>
          <TabPanel width="full">
            {posts
              ? posts.map((post: PostType) => (
                  <Post
                    key={post.id}
                    post={post}
                    onEditClick={() => console.log('Edit post:', post.id)}
                    onDeleteClick={() => console.log('Delete post:', post.id)}
                  />
                ))
              : 'No posts'}
            <Box>
              <Button
                colorScheme="blue"
                mr="2"
                onClick={handlePreviousClick}
                isDisabled={page === 1}
              >
                Previous
              </Button>
              <Button
                colorScheme="blue"
                onClick={handleNextClick}
                isDisabled={posts.length < 25}
              >
                Next
              </Button>
            </Box>
          </TabPanel>
          <TabPanel width="full">
            <TableContainer>
              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Th>Id</Th>
                    <Th>Username</Th>
                    <Th>Name</Th>
                    <Th>Email</Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {users
                    ? users.map((user: UserType) => (
                        <User
                          onClick={() => handleUserClick(user.id)}
                          key={user.id}
                          user={user}
                        />
                      ))
                    : 'No users'}
                </Tbody>
              </Table>
            </TableContainer>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Flex>
  )
}

export default Posts
