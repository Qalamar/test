import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, screen, fireEvent } from '@testing-library/react'
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import Posts from './Posts'

const queryClient = new QueryClient()

const server = setupServer(
  rest.get('https://jsonplaceholder.typicode.com/posts', (req, res, ctx) => {
    const page = Number(req.url.searchParams.get('_page'))
    const limit = Number(req.url.searchParams.get('_limit'))
    const searchText = req.url.searchParams.get('q')
    const startIndex = (page - 1) * limit
    const endIndex = startIndex + limit
    const filteredPosts = searchText
      ? posts.filter(post => post.body.includes(searchText))
      : posts

    return res(
      ctx.status(200),
      ctx.json({
        data: filteredPosts.slice(startIndex, endIndex),
        total: filteredPosts.length,
      }),
    )
  }),
  rest.get('https://jsonplaceholder.typicode.com/users', (req, res, ctx) => {
    const userId = Number(req.url.searchParams.get('userId'))
    const filteredUsers = userId
      ? users.filter(user => user.id === userId)
      : users

    return res(ctx.status(200), ctx.json({ data: filteredUsers }))
  }),
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

const posts = [
  {
    userId: 1,
    id: 1,
    title: 'title 1',
    body: 'body 1',
  },
  {
    userId: 1,
    id: 2,
    title: 'title 2',
    body: 'body 2',
  },
  {
    userId: 1,
    id: 3,
    title: 'title 3',
    body: 'body 3',
  },
]

const users = [
  {
    id: 1,
    name: 'User 1',
    username: 'user1',
    email: 'user1@example.com',
  },
  {
    id: 2,
    name: 'User 2',
    username: 'user2',
    email: 'user2@example.com',
  },
]

test('renders Posts component', async () => {
  render(
    <QueryClientProvider client={queryClient}>
      <Posts />
    </QueryClientProvider>,
  )

  expect(await screen.findByText('Loading...')).toBeInTheDocument()

  const articlesTab = screen.getByText('Articles')
  fireEvent.click(articlesTab)

  expect(await screen.findByText('No posts')).toBeInTheDocument()

  const usersTab = screen.getByText('Users')
  fireEvent.click(usersTab)

  expect(await screen.findByText('No Users')).toBeInTheDocument()
})

test('displays error message when data fetching fails', async () => {
  server.use(
    rest.get('https://jsonplaceholder.typicode.com/posts', (req, res, ctx) => {
      return res(ctx.status(500))
    }),
    rest.get('https://jsonplaceholder.typicode.com/users', (req, res, ctx) => {
      return res(ctx.status(500))
    }),
  )

  render(
    <QueryClientProvider client={queryClient}>
      <Posts />
    </QueryClientProvider>,
  )

  expect(await screen.findByText('Error')).toBeInTheDocument()
})
